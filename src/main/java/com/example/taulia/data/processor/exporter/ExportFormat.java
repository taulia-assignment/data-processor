package com.example.taulia.data.processor.exporter;

public enum ExportFormat {
    CSV(".csv"),
    XML(".xml");

    String extension;

    ExportFormat(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}
