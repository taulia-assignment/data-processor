package com.example.taulia.data.processor.exporter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public interface ReportExporter<T> {

    void export(String fileName, T data, boolean append);

    default Path buildPath(String fileName, ExportFormat format) {
        return Paths.get(System.getProperty("user.home") +
                File.separator +
                fileName +
                format.getExtension());
    }
}
