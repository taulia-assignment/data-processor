package com.example.taulia.data.processor.exporter.csv;

import com.example.taulia.data.processor.exporter.ExportFormat;
import com.example.taulia.data.processor.exporter.ReportExporter;
import com.example.taulia.data.processor.model.csv.CsvReportModel;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvExporter implements ReportExporter<CsvReportModel> {
    private static final String[] NAME_MAPPINGS = Arrays.stream(CsvReportModel.class.getDeclaredFields())
            .map(Field::getName)
            .collect(Collectors.toList())
            .toArray(new String[CsvReportModel.class.getDeclaredFields().length]);

    private static final String[] HEADERS = Arrays.stream(NAME_MAPPINGS)
            .map(nameMapping -> nameMapping.replaceAll("([A-Z])", "_$1").toLowerCase())
            .collect(Collectors.toList())
            .toArray(new String[NAME_MAPPINGS.length]);

    private static final CellProcessor[] DEFAULT_PROCESSORS = Stream.generate(Optional::new)
            .limit(NAME_MAPPINGS.length)
            .collect(Collectors.toList())
            .toArray(new CellProcessor[NAME_MAPPINGS.length]);

    public void export(String fileName, CsvReportModel model, boolean append) {
        final Path path = buildPath(fileName, ExportFormat.CSV);
        try (ICsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(path.toString(), append),
                CsvPreference.STANDARD_PREFERENCE)) {
            if (!Files.exists(path) || Files.size(path) == 0)
                beanWriter.writeHeader(HEADERS);

            beanWriter.write(model, NAME_MAPPINGS, DEFAULT_PROCESSORS);
        } catch (IOException e) {
            System.out.println("Problem occurred while creating CSV file: " + e.getMessage());
        }
    }
}
