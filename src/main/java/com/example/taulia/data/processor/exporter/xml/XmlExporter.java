package com.example.taulia.data.processor.exporter.xml;

import com.example.taulia.data.processor.exporter.ExportFormat;
import com.example.taulia.data.processor.exporter.ReportExporter;
import com.example.taulia.data.processor.model.xml.InvoiceDetails;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class XmlExporter implements ReportExporter<InvoiceDetails> {

    public void export(String fileName, InvoiceDetails model, boolean append) {
        final File file = new File(buildPath(fileName, ExportFormat.XML).toString());
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(XmlRootContainer.class);
            XmlRootContainer root = new XmlRootContainer();
            if (!file.createNewFile()) {
                final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                root = (XmlRootContainer) unmarshaller.unmarshal(file);

            }

            final ArrayList<InvoiceDetails> invoiceDetails = new ArrayList<>(root.getInvoiceDetails());
            invoiceDetails.add(model);

            root.setInvoiceDetails(invoiceDetails);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(root, file);
        } catch (JAXBException | IOException e) {
            System.out.println("Problem occurred while creating XML file: " + e.getMessage());
        }
    }
}
