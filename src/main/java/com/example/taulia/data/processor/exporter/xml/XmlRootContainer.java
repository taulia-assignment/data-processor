package com.example.taulia.data.processor.exporter.xml;

import com.example.taulia.data.processor.model.xml.InvoiceDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "invoices")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRootContainer {

    @XmlElement(name = "invoiceDetails")
    private List<InvoiceDetails> invoiceDetails;

    public List<InvoiceDetails> getInvoiceDetails() {
        if (invoiceDetails == null)
            return new ArrayList<>();

        return invoiceDetails;
    }

    public void setInvoiceDetails(List<InvoiceDetails> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }
}
