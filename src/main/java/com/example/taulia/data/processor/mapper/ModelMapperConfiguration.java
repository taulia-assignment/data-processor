package com.example.taulia.data.processor.mapper;

import com.example.taulia.data.processor.model.csv.CsvRecord;
import com.example.taulia.data.processor.model.csv.CsvReportModel;
import com.example.taulia.data.processor.model.xml.InvoiceAmount;
import com.example.taulia.data.processor.model.xml.InvoiceCurrency;
import com.example.taulia.data.processor.model.xml.InvoiceDetails;
import com.example.taulia.data.processor.model.xml.InvoiceDueDate;
import com.example.taulia.data.processor.model.xml.InvoiceStatus;
import com.example.taulia.data.processor.model.xml.Supplier;
import lombok.Getter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;

@Getter
public class ModelMapperConfiguration {

    private ModelMapper modelMapper;

    public ModelMapperConfiguration() {
        modelMapper = new ModelMapper();
        configureCsvRecordToCsvReportModel();
        configureCsvRecordToInvoiceModel();
        configureStringArrayToCsvRecord();
    }

    private void configureCsvRecordToCsvReportModel() {
        TypeMap<CsvRecord, CsvReportModel> typeMap = modelMapper.getTypeMap(CsvRecord.class, CsvReportModel.class);
        if (typeMap == null)
            typeMap = modelMapper.createTypeMap(CsvRecord.class, CsvReportModel.class);

        Converter<CsvRecord, CsvReportModel> converter =
                ctx -> {
                    CsvRecord csvRecord = ctx.getSource();

                    return CsvReportModel.builder()
                            .imageName(csvRecord.getImageName())
                            .invoiceImage(csvRecord.getInvoiceImage())
                            .invoiceAmount(csvRecord.getInvoiceAmount())
                            .invoiceDueDate(csvRecord.getInvoiceDueDate())
                            .invoiceNumber(csvRecord.getInvoiceNumber())
                            .invoiceCurrency(csvRecord.getInvoiceCurrency())
                            .invoiceStatus(csvRecord.getInvoiceStatus())
                            .supplier(csvRecord.getSupplier())
                            .build();
                };

        typeMap.setConverter(converter);
    }

    private void configureCsvRecordToInvoiceModel() {
        TypeMap<CsvRecord, InvoiceDetails> typeMap = modelMapper.getTypeMap(CsvRecord.class, InvoiceDetails.class);
        if (typeMap == null)
            typeMap = modelMapper.createTypeMap(CsvRecord.class, InvoiceDetails.class);

        Converter<CsvRecord, InvoiceDetails> converter =
                ctx -> {
                    CsvRecord csvRecord = ctx.getSource();

                    final InvoiceDueDate invoiceDueDate = new InvoiceDueDate();
                    invoiceDueDate.setDate(csvRecord.getInvoiceDueDate());
                    final InvoiceAmount invoiceAmount = new InvoiceAmount();
                    invoiceAmount.setValue(csvRecord.getInvoiceAmount());
                    final InvoiceCurrency invoiceCurrency = new InvoiceCurrency();
                    invoiceCurrency.setValue(csvRecord.getInvoiceCurrency());
                    final InvoiceStatus invoiceStatus = new InvoiceStatus();
                    invoiceStatus.setValue(csvRecord.getInvoiceStatus());
                    final Supplier supplier = new Supplier();
                    supplier.setName(csvRecord.getSupplier());
                    return InvoiceDetails.builder()
                            .number(csvRecord.getInvoiceNumber())
                            .invoiceDueDate(invoiceDueDate)
                            .invoiceAmount(invoiceAmount)
                            .invoiceCurrency(invoiceCurrency)
                            .invoiceStatus(invoiceStatus)
                            .supplier(supplier)
                            .build();
                };

        typeMap.setConverter(converter);
    }

    private void configureStringArrayToCsvRecord() {
        TypeMap<String[], CsvRecord> typeMap = modelMapper.getTypeMap(String[].class, CsvRecord.class);
        if (typeMap == null)
            typeMap = modelMapper.createTypeMap(String[].class, CsvRecord.class);

        Converter<String[], CsvRecord> converter =
                ctx -> {
                    String[] data = ctx.getSource();

                    return CsvRecord.builder()
                            .buyer(data[0])
                            .imageName(data[1])
                            .invoiceImage(data[2])
                            .invoiceDueDate(data[3])
                            .invoiceNumber(data[4])
                            .invoiceAmount(data[5])
                            .invoiceCurrency(data[6])
                            .invoiceStatus(data[7])
                            .supplier(data[8])
                            .build();
                };

        typeMap.setConverter(converter);
    }
}
