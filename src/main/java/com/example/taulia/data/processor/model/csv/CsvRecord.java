package com.example.taulia.data.processor.model.csv;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CsvRecord {
    private String buyer;
    private String imageName;
    private String invoiceImage;
    private String invoiceDueDate;
    private String invoiceNumber;
    private String invoiceAmount;
    private String invoiceCurrency;
    private String invoiceStatus;
    private String supplier;
}
