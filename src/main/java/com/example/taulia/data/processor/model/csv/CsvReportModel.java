package com.example.taulia.data.processor.model.csv;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
public class CsvReportModel {
    private String imageName;
    private String invoiceImage;
    private String invoiceDueDate;
    private String invoiceNumber;
    private String invoiceAmount;
    private String invoiceCurrency;
    private String invoiceStatus;
    private String supplier;
}
