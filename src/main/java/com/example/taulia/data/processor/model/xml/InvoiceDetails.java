package com.example.taulia.data.processor.model.xml;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "invoiceDetails")
public class InvoiceDetails {
    @XmlAttribute(name = "number")
    private String number;

    @XmlElement(name = "dueDate")
    private InvoiceDueDate invoiceDueDate;

    @XmlElement(name = "amount")
    private InvoiceAmount invoiceAmount;

    @XmlElement(name = "currency")
    private InvoiceCurrency invoiceCurrency;

    @XmlElement(name = "status")
    private InvoiceStatus invoiceStatus;

    @XmlElement(name = "supplier")
    private Supplier supplier;
}
