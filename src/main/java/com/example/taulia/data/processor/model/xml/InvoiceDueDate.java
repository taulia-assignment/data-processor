package com.example.taulia.data.processor.model.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name = "dueDate")
@XmlAccessorType(XmlAccessType.FIELD)
public class InvoiceDueDate {
    @XmlAttribute(name = "date")
    private String date;
}
