package com.example.taulia.data.processor.parser;

import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CsvParser implements Parser {

    private ModelMapper modelMapper;

    public CsvParser(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public <T> void parse(String path, Class<T> mapTo, Consumer<T> compute) throws IOException {
        try (Stream<String> streamLines = Files.lines(Paths.get(path))) {
            streamLines.skip(1)
                    .map(line -> modelMapper.map(line.split(","), mapTo))
                    .forEach(compute);
        }
    }
}
