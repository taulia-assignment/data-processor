package com.example.taulia.data.processor.parser;

import java.io.IOException;
import java.util.function.Consumer;

public interface Parser {
    <T> void parse(String path, Class<T> mapTo, Consumer<T> compute) throws IOException;
}
