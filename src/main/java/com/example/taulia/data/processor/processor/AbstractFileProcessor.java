package com.example.taulia.data.processor.processor;

import com.example.taulia.data.processor.exporter.ReportExporter;
import com.example.taulia.data.processor.model.csv.CsvRecord;
import com.example.taulia.data.processor.parser.Parser;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public abstract class AbstractFileProcessor<T> implements FileProcessor {

    private final Parser parser;
    private final ModelMapper modelMapper;
    private final Class<T> type;

    abstract ReportExporter<T> getExporter();

    public AbstractFileProcessor(Parser parser,
                                 ModelMapper modelMapper,
                                 Class<T> type) {
        this.parser = parser;
        this.modelMapper = modelMapper;
        this.type = type;
    }

    @Override
    public void process(String filePath) throws IOException {
        System.out.println("Start processing your file...");

        parser.parse(filePath,
                CsvRecord.class,
                csvRecord -> getExporter().export(csvRecord.getBuyer(),
                        modelMapper.map(csvRecord, type),
                        true));

        System.out.println("Done. Processed data can be found under your user's home directory.");
    }
}
