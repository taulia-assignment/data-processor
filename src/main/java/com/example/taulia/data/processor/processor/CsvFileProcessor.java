package com.example.taulia.data.processor.processor;

import com.example.taulia.data.processor.exporter.ReportExporter;
import com.example.taulia.data.processor.exporter.csv.CsvExporter;
import com.example.taulia.data.processor.mapper.ModelMapperConfiguration;
import com.example.taulia.data.processor.model.csv.CsvReportModel;
import com.example.taulia.data.processor.parser.CsvParser;

public class CsvFileProcessor extends AbstractFileProcessor<CsvReportModel> implements FileProcessor {

    private final ReportExporter<CsvReportModel> exporter;

    public CsvFileProcessor(ModelMapperConfiguration modelMapperConfiguration) {
        super(new CsvParser(modelMapperConfiguration.getModelMapper()),
                modelMapperConfiguration.getModelMapper(),
                CsvReportModel.class);

        exporter = new CsvExporter();
    }

    @Override
    ReportExporter<CsvReportModel> getExporter() {
        return exporter;
    }
}
