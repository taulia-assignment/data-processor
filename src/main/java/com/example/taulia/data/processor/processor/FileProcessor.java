package com.example.taulia.data.processor.processor;

import java.io.IOException;

public interface FileProcessor {
    void process(String filePath) throws IOException;
}

