package com.example.taulia.data.processor.processor;

import com.example.taulia.data.processor.exporter.ReportExporter;
import com.example.taulia.data.processor.exporter.xml.XmlExporter;
import com.example.taulia.data.processor.mapper.ModelMapperConfiguration;
import com.example.taulia.data.processor.model.xml.InvoiceDetails;
import com.example.taulia.data.processor.parser.CsvParser;

public class XmlFileProcessor extends AbstractFileProcessor<InvoiceDetails> implements FileProcessor {

    private final ReportExporter<InvoiceDetails> exporter;

    public XmlFileProcessor(ModelMapperConfiguration modelMapperConfiguration) {
        super(new CsvParser(modelMapperConfiguration.getModelMapper()),
                modelMapperConfiguration.getModelMapper(),
                InvoiceDetails.class);

        this.exporter = new XmlExporter();
    }

    @Override
    ReportExporter<InvoiceDetails> getExporter() {
        return exporter;
    }
}
